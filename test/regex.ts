import { expect } from 'chai';
import { Regex } from '../regex';

describe('Infix to Postfix', () => {
    it('empty & empty', () => {
        const matcher = Regex.createMatcher("");
        expect(matcher("")).to.be.true;
    });
    it('empty & a', () => {
        const matcher = Regex.createMatcher("");
        expect(matcher("a")).to.be.false;
    });
    it('empty & aa', () => {
        const matcher = Regex.createMatcher("");
        expect(matcher("a")).to.be.false;
    });
    it('a & a', () => {
        const matcher = Regex.createMatcher("a");
        expect(matcher("a")).to.be.true;
    });
    it('a & empty', () => {
        const matcher = Regex.createMatcher("a");
        expect(matcher("")).to.be.false;
    });
    it('a & b', () => {
        const matcher = Regex.createMatcher("a");
        expect(matcher("b")).to.be.false;
    });
    it('a & ab', () => {
        const matcher = Regex.createMatcher("a");
        expect(matcher("ab")).to.be.false;
    });
    it('a* & empty', () => {
        const matcher = Regex.createMatcher("a*");
        expect(matcher("")).to.be.true;
    });
    it('a* & a', () => {
        const matcher = Regex.createMatcher("a*");
        expect(matcher("a")).to.be.true;
    });
    it('a* & aa', () => {
        const matcher = Regex.createMatcher("a*");
        expect(matcher("aa")).to.be.true;
    });
    it('a* & aaa', () => {
        const matcher = Regex.createMatcher("a*");
        expect(matcher("aaa")).to.be.true;
    });
    it('a* & aab', () => {
        const matcher = Regex.createMatcher("a*");
        expect(matcher("aab")).to.be.false;
    });
    it('a+ & empty', () => {
        const matcher = Regex.createMatcher("a+");
        expect(matcher("")).to.be.false;
    });
    it('a+ & a', () => {
        const matcher = Regex.createMatcher("a+");
        expect(matcher("a")).to.be.true;
    });
    it('a+ & aa', () => {
        const matcher = Regex.createMatcher("a+");
        expect(matcher("aa")).to.be.true;
    });
    it('a+ & aaa', () => {
        const matcher = Regex.createMatcher("a+");
        expect(matcher("aaa")).to.be.true;
    });
    it('a+ & aab', () => {
        const matcher = Regex.createMatcher("a+");
        expect(matcher("aab")).to.be.false;
    });
    it('a|b & empty', () => {
        const matcher = Regex.createMatcher("a|b");
        expect(matcher("")).to.be.false;
    });
    it('a|b & a', () => {
        const matcher = Regex.createMatcher("a|b");
        expect(matcher("a")).to.be.true;
    });
    it('a|b & b', () => {
        const matcher = Regex.createMatcher("a|b");
        expect(matcher("b")).to.be.true;
    });
    it('a|b & ab', () => {
        const matcher = Regex.createMatcher("a|b");
        expect(matcher("ab")).to.be.false;
    });
    it('a|b & ba', () => {
        const matcher = Regex.createMatcher("a|b");
        expect(matcher("ba")).to.be.false;
    });
    it('(a|b)+ & a', () => {
        const matcher = Regex.createMatcher("(a|b)+");
        expect(matcher("a")).to.be.true;
    });
    it('(a|b)+ & ab', () => {
        const matcher = Regex.createMatcher("(a|b)+");
        expect(matcher("ab")).to.be.true;
    });
    it('(a|b)+ & aa', () => {
        const matcher = Regex.createMatcher("(a|b)+");
        expect(matcher("aa")).to.be.true;
    });
    it('(a|b)+ & bb', () => {
        const matcher = Regex.createMatcher("(a|b)+");
        expect(matcher("bb")).to.be.true;
    });
    it('(a|b)+ & bbaabbaa', () => {
        const matcher = Regex.createMatcher("(a|b)+");
        expect(matcher("bbaabbaa")).to.be.true;
    });
    it('ab* & ab', () => {
        const matcher = Regex.createMatcher("a.b*");
        expect(matcher("ab")).to.be.true;
    });
});