import { expect } from 'chai';
import { Postfix } from '../postfix';

describe('Infix to Postfix', () => {
    it('empty', () => {
        expect(Postfix.infixToPostfix("")).equal("");
    });
    it('single symbol', () => {
        expect(Postfix.infixToPostfix("a")).equal("a");
    });
    it('concat symbol', () => {
        expect(Postfix.infixToPostfix("a.b")).equal("ab.");
    });
    it('or symbol', () => {
        expect(Postfix.infixToPostfix("a|b")).equal("ab|");
    });
    it('kleene star symbol', () => {
        expect(Postfix.infixToPostfix("a*")).equal("a*");
    });
    it('kleene star symbol in concat', () => {
        expect(Postfix.infixToPostfix("a*.b")).equal("a*b.");
    });
    it('kleene star symbol in union', () => {
        expect(Postfix.infixToPostfix("a*|b")).equal("a*b|");
    });
    it('oneOrMore symbol', () => {
        expect(Postfix.infixToPostfix("a+")).equal("a+");
    });
    it('oneOrMore symbol in concat', () => {
        expect(Postfix.infixToPostfix("a+.b")).equal("a+b.");
    });
    it('oneOrMore symbol in union', () => {
        expect(Postfix.infixToPostfix("a+|b")).equal("a+b|");
    });

    it('parenthesis', () => {
        expect(Postfix.infixToPostfix("(a)")).equal("a");
    });
    it('parenthesis union', () => {
        expect(Postfix.infixToPostfix("(a|b)|c")).equal("ab|c|");
    });
    it('parenthesis union union concat', () => {
        expect(Postfix.infixToPostfix("((a|b)|c).d")).equal("ab|c|d.");
    });

});