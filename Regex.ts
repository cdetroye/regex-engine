import { NFA } from "./nfa";
import { State } from "./state";
import { Postfix } from "./postfix";

export class Regex {
    static search(nfa: NFA, word: String): boolean {
        let currentStates = [];
        Regex.addNextState(nfa.start, currentStates, []);

        for (const symbol of word) {
            const nextStates = [];
            for (const state of currentStates) {
                const nextState = state.transition[symbol];
                if (nextState) {
                    Regex.addNextState(nextState, nextStates, []);
                }
            }
            currentStates = nextStates;
        }
        return currentStates.find(s => s.final) ? true : false;
    }

    static addNextState(state: State, nextStates: State[], visited: State[]): void {
        if (state.epsilonTransitions.length) {
            for (const st of state.epsilonTransitions) {
                if (!visited.find(vs => vs === st)) {
                    visited.push(st);
                    Regex.addNextState(st, nextStates, visited);
                }
            }
        } else {
            nextStates.push(state);
        }
    }

    static createMatcher(exp: string) {
        const postfix = Postfix.infixToPostfix(exp);
        const nfa = NFA.parseNFA(postfix);
        return (word: String) => Regex.search(nfa, word);
    }
}
