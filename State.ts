// https://deniskyashif.com/2019/02/17/implementing-a-regular-expression-engine/
export class State {
    final: boolean;
    transition: { [x: string]: State; };
    epsilonTransitions: State[];

    constructor(final: boolean) {
        this.final = final;
        this.transition = {};
        this.epsilonTransitions = [];

    }

    addEpsilonTransition(to: State) {
        this.epsilonTransitions.push(to);
    }

    addTransition(to: State, symbol: string) {
        this.transition[symbol] = to;
    }
}
