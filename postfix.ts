const operatorPrecedence = {
    '|': 0,
    '.': 1,
    '?': 2,
    '*': 2,
    '+': 2
};

function isOperator(c: string): boolean {
    return ['|', '.', '?', '*', '+'].includes(c);
}

export class Postfix {
    // Shunting Yard
    // https://en.wikipedia.org/wiki/Shunting-yard_algorithm#A_simple_conversion
    static infixToPostfix(exp: string): string {
        var operatorStack = [];
        var outputQueue = [];

        for (const c of exp) {
            if (isOperator(c)) {
                if (operatorStack.length > 0 && operatorStack.at(-1) != '(' && operatorPrecedence[operatorStack.at(-1)] >= operatorPrecedence[c]) {
                    outputQueue.push(operatorStack.pop());
                }
                operatorStack.push(c);
            }
            else if (c === '(') {
                operatorStack.push(c);
            }
            else if (c === ')') {
                while (operatorStack.length > 0 && operatorStack.at(-1) != '(') {
                    outputQueue.push(operatorStack.pop());
                }
                // There should be a left parens on the operator stack right now.
                operatorStack.pop();
            }
            else {
                outputQueue.push(c);
            }

        }

        while (operatorStack.length > 0) {
            outputQueue.push(operatorStack.pop());

        }
        return outputQueue.join("");
    }
}
