import { State } from "./state";
import * as _ from 'lodash';    
export class NFA {
    start: State;
    end: State;

    constructor(start: State, end: State) {
        this.start = start;
        this.end = end;
    }

    static fromSymbol(symbol: string): NFA {
        const start = new State(false);
        const end = new State(true);
        start.addTransition(end, symbol);

        return new NFA(start, end);
    }

    static fromEpsilon(): NFA {
        const start = new State(false);
        const end = new State(true);
        start.addEpsilonTransition(end);

        return new NFA(start, end);
    }

    static concat(first: NFA, second: NFA): NFA {
        first.end.addEpsilonTransition(second.start);
        first.end.final = false;

        return new NFA(first.start, second.end);
    }

    static union(first: NFA, second: NFA): NFA {
        const start = new State(false);
        const end = new State(true);

        start.addEpsilonTransition(first.start);
        start.addEpsilonTransition(second.start);

        first.end.addEpsilonTransition(end);
        first.end.final = false;
        second.end.addEpsilonTransition(end);
        second.end.final = false;

        return new NFA(start, end);
    }

    static atLeastOne(nfa: NFA): NFA {
        const cloned = _.cloneDeep(nfa);
        const zeroOrMore = NFA.closure(cloned);

        nfa.end.addEpsilonTransition(zeroOrMore.start);
        nfa.end.final = false;

        return new NFA(nfa.start, zeroOrMore.end); 
    }
    static closure(nfa: NFA): NFA {
        const start = new State(false);
        const end = new State(true);

        start.addEpsilonTransition(end);
        start.addEpsilonTransition(nfa.start);

        nfa.end.addEpsilonTransition(end);
        nfa.end.addEpsilonTransition(nfa.start);
        nfa.end.final = false;

        return new NFA(start, end);
    }

    static parseNFA(postfixExp: string): NFA {
        if (postfixExp === '') {
            return NFA.fromEpsilon();
        }
    
        const stack = [];
    
        for (const token of postfixExp) {
            if (token === '*') {
                stack.push(NFA.closure(stack.pop()));
            } else if (token === "+") {
                stack.push(NFA.atLeastOne(stack.pop()));
            }else if (token === "|") {
                const right = stack.pop();
                const left = stack.pop();
                stack.push(NFA.union(left, right));
            } else if (token === ".") {
                const right = stack.pop();
                const left = stack.pop();
                stack.push(NFA.concat(left, right));
            } else {
                stack.push(NFA.fromSymbol(token));
            }
        }
    
        return stack.pop();
    }
}